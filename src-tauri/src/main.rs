#![cfg_attr(
  all(not(debug_assertions), target_os = "windows"),
  windows_subsystem = "windows"
)]

use serde::Serialize;

type Result<T> = std::result::Result<T, String>;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub enum DirEntryKind {
  File, Folder
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DirEntry {
  pub kind: DirEntryKind,
  pub path: String,
  pub display_name: String,
}

#[tauri::command]
fn get_cwd() -> Result<String> {
  Ok(std::env::current_dir()
    .map_err(|e| e.to_string())?
    .to_str()
    .ok_or("Current directory is not valid UTF-8")?
    .to_string())
}

#[tauri::command]
fn list_files_in_dir(path: &str) -> Result<Vec<DirEntry>> {
  let mut children: Vec<DirEntry> = Vec::new();
  camino::Utf8Path::new(path)
    .read_dir_utf8()
    .map_err(|e| e.to_string())?
    .try_for_each(|p| match p {
      Ok(path) => Ok(children.push(DirEntry{
        kind: match path.file_type() {
          Ok(ft) if ft.is_dir() => DirEntryKind::Folder,
          _ => DirEntryKind::File
        },
        display_name: path.file_name().to_string(),
        path: path.path().to_string()
      })),
      Err(e) => Err(e)
    })
    .map_err(|e| e.to_string())?;
  Ok(children)
}

#[tauri::command]
fn get_file_info(path: &str) -> Result<Vec<dango::JSONIdentifyLayer>> {
  let path = camino::Utf8Path::new(path);
  dango::detect_path_v2(&path).map_err(|e| e.to_string())
}

fn main() {
  let context = tauri::generate_context!();
  tauri::Builder::default()
    .menu(if cfg!(target_os = "macos") {
      tauri::Menu::os_default(&context.package_info().name)
    } else {
      tauri::Menu::default()
    })
    .invoke_handler(tauri::generate_handler![get_cwd, list_files_in_dir, get_file_info])
    .run(context)
    .expect("error while running tauri application");
}
